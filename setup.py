from setuptools import setup

setup(
    name="laundry",
    version="0.1",
    packages=[
        'laundry'
    ],
    requires=[
        'pyzmq',
        'ouimeaux'
    ]
)
