import logging ;
import subprocess

log = logging.getLogger(__name__)


class WeMoIndicator(object):
    """ Uses the commandline wemo from ouimeaux
    """

    def __init__(self, switchname):
        self._expected_switch = switchname

    def start(self):
        pass

    def _wemo(self, command):
        args = [
            "wemo",
            "switch",
            self._expected_switch,
            command if command in ( "on", "off" ) else "toggle"
        ]

        return subprocess.call(args) == 0

    def on(self):
        return self._wemo("on")

    def off(self):
        return self._wemo("off")