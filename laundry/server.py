import json
import logging
import os
import os.path as path
from laundry import client
from laundry.pin_indicator import PinIndicator
from laundry.wemo_indicator import WeMoIndicator

log = logging.getLogger(__name__)

import threading
import zmq
from pyfirmata import Arduino, util

import command

class LazyPirateServer(threading.Thread):
    def __init__(self, port, cmd_runner):
        super(LazyPirateServer, self).__init__()
        self.daemon = True
        self._port = port
        self._runner = cmd_runner

    def run(self):
        ctx = zmq.Context()
        socket = ctx.socket(zmq.REP)  # Response Socket
        socket.bind("tcp://*:{0}".format(self._port))

        try:
            log.info("Starting Server")
            while True:
                request = json.loads(socket.recv())
                log.debug("< {0}".format(request))
                response = json.dumps(self._runner(request))
                log.debug("> {0}".format(response))
                socket.send(response)  # Echo back what the message was
        except KeyboardInterrupt:
            pass
        except Exception, ex:
            print "Exception Caught: {0}".format(ex)
        finally:
            socket.close()
            ctx.term()


class Indicator(object):
    def on(self):
        log.debug("Switching On")

    def off(self):
        log.debug("Switching Off")

class InvertedIndicator(object):
    def __init__(self, indicator):
        self._parent = indicator

    def start(self):
        self._parent.start()

    def on(self):
        self._parent.off()

    def off(self):
        self._parent.on()

class MultiIndicator(object):
    def __init__(self, *args):
        self._indicators = args

    def start(self):
        map(lambda x: x.start(), self._indicators)

    def on(self):
        map(lambda x: x.on(), self._indicators)

    def off(self):
        map(lambda x: x.off(), self._indicators)

class PointDetector(object):
    def __init__(self, pin, switch):
        self._on = 1
        self._off = 0
        self.status = False
        self._pin = pin
        self._switch = switch
        self._calibration_file = self._determine_calibration_file()
        self._load_calibration()

        self._pin.callback = self._detect

    def _detect(self, value):
        log.debug("New Sensor Value: {0}".format(value))
        if value >= self._on and not self.status:
            self.status = True
            self._switch.on()
        if value <= self._off and self.status:
            self.status = False
            self._switch.off()

    def _set_on(self, value):
        self._on = value
        self._save_calibration()

    def _set_off(self, value):
        self._off = value
        self._save_calibration()

    def _get_on(self):
        return self._on

    def _get_off(self):
        return self._off

    on = property(_get_on, _set_on)
    off = property(_get_off, _set_off)

    def _load_calibration(self):
        if path.exists(self._calibration_file):
            log.info("Loading Calibration Data")
            data = json.load(open(self._calibration_file))
            if "on" in data:
                self._on = float(data["on"])
            if "off" in data:
                self._off = float(data["off"])

    def _save_calibration(self):
        if not path.exists(path.dirname(self._calibration_file)):
            try:
                os.makedirs(path.dirname(self._calibration_file))
            except IOError:
                pass
            except WindowsError:
                pass

        json.dump({
                      "on": self._on,
                      "off": self._off
                  }, open(self._calibration_file, "w"))

    def _determine_calibration_file(self):
        if "LAUNDRY_CALIBRATION_FILE" in os.environ:
            return os.environ['LAUNDRY_CALIBRATION_FILE']

        return path.join(path.expanduser('~'), 'laundry_calibration.json')


class LaundryServer(object):
    def __init__(self, comport, port):
        self._port = port
        self._zmq_thread = LazyPirateServer(port, self._command_processor)
        self._firmata = Arduino(comport)
        self._indicator_pin = self._firmata.get_pin("a:0:i")
        self._led_pin = self._firmata.get_pin("d:13:o")
        self._indicator = InvertedIndicator(
            MultiIndicator(
                WeMoIndicator("laundrylight"),
                PinIndicator(self._led_pin))
        )
        self._detector = PointDetector(self._indicator_pin, self._indicator)

    def start(self):
        # Start Firmata
        self._iterator = util.Iterator(self._firmata)
        self._iterator.start()
        self._indicator_pin.enable_reporting()

        # Start Zmq
        self._zmq_thread.start()

        self._indicator.start()


    def _command_processor(self, data):
        cmd = data["command_name"]
        del data["command_name"]

        func = getattr(self, "_do_{0}".format(cmd), self._do_error)
        return func(cmd, **data)

    def _do_error(self, name, **kwargs):
        return {
            'error': "Command Not Found",
            'command_name': name,
            'args': kwargs
        }

    def _do_exit(self, _):
        self._firmata.exit()

    def _do_sensor_get(self, _):
        return {
            "value": "{0}".format(
                self._indicator_pin.read()
            )
        }

    def _do_sensor_calibrate(self, name, point=""):
        if point == "on":
            self._detector.on = self._indicator_pin.read()
        elif point == "off":
            self._detector.off = self._indicator_pin.read()
        elif point == "show":
            return {
                "on_threshold": self._detector.on,
                "off_threshold": self._detector.off
            }
        else:
            return self._do_error(name, point=point)

        return "OK"

    def _do_hw_firmware(self, _, **kwargs):
        return {
            'firmware': {
                'name': self._firmata.firmware,
                'version': self._firmata.firmware_version,
            },
            'firmata_version': self._firmata.firmata_version,
        }

    def _do_switch_state(self, _):
        return {
            "indicator": {True: "On", False: "Off"}[self._detector.status]
        }

    def _do_set_debug(self, _, state):
        level = logging.DEBUG if state == "on" else logging.INFO
        logging.getLogger().setLevel(level)
        return "OK"


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    server = LaundryServer("com3:", 5555)
    server.start()

    x = command.DynamicRepl(path.join(path.dirname(__file__), "commands.json"), client.Protocol(5555))
    x.cmdloop()