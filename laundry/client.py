import json
import logging
import inspect

log = logging.getLogger(__name__)

import zmq


def _me():
    return inspect.stack()[1][0]


class ServerNotResponding(Exception):
    pass


class Protocol(object):
    REQUEST_RETRIES = 3
    REQUEST_TIMEOUT = 2500

    def __init__(self, port):
        self._port = port
        self._ctx = zmq.Context()
        self._poll = zmq.Poller()
        self._connect_socket()


    def _connect_socket(self):
        self._socket = self._ctx.socket(zmq.REQ)
        self._socket.connect("tcp://127.0.0.1:{0}".format(self._port))
        self._poll.register(self._socket, zmq.POLLIN)

    def send(self, calldict):
        return self._lazy_pirate(json.dumps(calldict))

    def _lazy_pirate(self, msg):
        retries_left = self.REQUEST_RETRIES
        request = str(msg)
        while retries_left:
            self._socket.send(request)

            expect_reply = True
            while expect_reply:
                socks = dict(self._poll.poll(self.REQUEST_TIMEOUT))
                if socks.get(self._socket) == zmq.POLLIN:
                    reply = self._socket.recv()
                    return reply
                else:
                    log.warning("No Response from server, retrying...")
                    self._socket.setsockopt(zmq.LINGER, 0)
                    self._socket.close()
                    self._poll.unregister(self._socket)
                    retries_left -= 1
                    if retries_left == 0:
                        log.error("Server did not respond in sufficient time")
                        return

                    self._connect_socket()
                    self._socket.send(request)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    x = Protocol(5555)
    print x._lazy_pirate("test message")