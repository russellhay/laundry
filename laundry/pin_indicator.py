import logging
log = logging.getLogger(__name__)

class PinIndicator(object):
    """ Uses an arduino pin as the indicator
    """

    def __init__(self, pin):
        self._pin = pin

    def start(self):
        pass

    def on(self):
        log.info("LED ON")
        return self._pin.write(1)

    def off(self):
        log.info("LED OFF")
        return self._pin.write(0)