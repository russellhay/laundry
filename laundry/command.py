import cmd
import json
import logging
import types
import functools
import pprint

from laundry import client


class DynamicRepl(cmd.Cmd, object):
    def __init__(self, cmds_or_jsonfile, protocol):
        self._protocol = protocol
        if isinstance(cmds_or_jsonfile, types.StringType):
            commands = json.load(open(cmds_or_jsonfile, "r"))
        else:
            commands = cmds_or_jsonfile

        self._setup_commands(commands)

        super(DynamicRepl, self).__init__()

    def can_exit(self):
        return True

    def do_exit(self, s):
        self._protocol.send(
            { "command_name": "exit" }
        )
        return True

    do_EOF = do_exit

    def get_names(self):
        """ Override the normal get_names and return all on this instance rather than on the class """
        return dir(self)

    def _setup_commands(self, commands):

        for top_level, data in commands.items():
            sub_commands = data['commands']
            help_message = data['help']
            setattr(self, "do_{0}".format(top_level),
                    functools.partial(self._run_command, top_level, sub_commands))
            getattr(self, "do_{0}".format(top_level)).__doc__ = help_message
            setattr(self, "help_{0}".format(top_level),
                    functools.partial(self._print_help, help_message, sub_commands))

    def _run_command(self, name, commands, str):
        if " " in str:
            subcmd, data = str.split(" ", 1)
        else:
            subcmd = str
            data = None

        if subcmd not in commands:
            print "Cannot find command {0} {1}".format(
                name, subcmd
            )
            return

        args = {
            'command_name': '{0}_{1}'.format(name, subcmd)
        }

        if "args" in commands[subcmd]:
            if data is None:
                print "Wrong number of arguments"
                return
            else:
                print "\t" + data
                cmd_args = commands[subcmd]["args"]
                sep_count = data.count(" ")
                if sep_count < len(cmd_args) - 1:
                    print "Wrong number of arguments"
                    return

                if sep_count == 1:
                    data = ( data, )
                else:
                    data = data.split(" ")

                args.update(dict(zip(cmd_args, data)))

        pprint.pprint(json.loads(self._protocol.send(args)), indent=2)

    def _print_help(self, msg, commands):
        print msg
        print
        print "Available Subcommands: "
        for command_name, command_data in commands.items():
            print "\t{0} - {1}".format(command_name, command_data["help"])


if __name__ == "__main__":
    import os.path as path

    logging.basicConfig(level=logging.DEBUG)

    x = DynamicRepl(path.join(path.dirname(__file__), "commands.json"), client.Protocol(5555))
    x.cmdloop()